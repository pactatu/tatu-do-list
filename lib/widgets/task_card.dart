import 'package:flutter/material.dart';
import 'package:pac/connections/delete.dart';
import 'package:pac/model/task_model.dart';
import 'package:pac/screens/edit_task.dart';
import 'package:pac/screens/main_page.dart';

class TaskCard extends StatelessWidget {
  final int id;
  final String title;
  final String description;
  final int userId;

  TaskCard(
      {required this.id,
      required this.title,
      required this.description,
      required this.userId});

  //TaskCard({required this.taskClass});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              boxShadow: [
                BoxShadow(
                  color: Color(0x3f000000),
                  blurRadius: 4,
                  offset: Offset(0, 4),
                ),
              ],
              color: Color(0xffebebeb),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                // Container(
                //   width: 100.0,
                //   decoration: BoxDecoration(
                //     borderRadius: BorderRadius.circular(5),
                //     color: Color(0xff36b0c1),
                //   ),
                // ),
                Container(
                  width: 200.0,
                  padding: EdgeInsets.all(25.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        title,
                        softWrap: true,
                        overflow: TextOverflow.clip,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 24,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        description,
                        softWrap: true,
                        overflow: TextOverflow.clip,
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  children: [
                    Container(
                      child: IconButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      EditTask(taskInfo: Task(id: this.id, name: this.title, content: this.description, userId: this.userId))),
                            );
                          },
                          icon: Icon(Icons.create),
                      ),
                    ),
                    Container(
                      child: IconButton(
                          onPressed: () {
                            AlertDialog alert = AlertDialog(
                              title: Text("Delete task"),
                              content: Text(
                                  "Are you sure you want to delete the task \"" +
                                      this.title +
                                      "\"?"),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.pop(context, "Cancel");
                                  },
                                  child: Text("Cancel"),
                                ),
                                TextButton(
                                  onPressed: () async {
                                    final response = await deleteTask(this.id);

                                    if (response) {
                                      showDialog(
                                          context: context,
                                          builder: (context) {
                                            return AlertDialog(
                                                title: Text(
                                                    "Deleted successfully"),
                                                actions: [
                                                  ElevatedButton(
                                                      onPressed: () {
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                              builder: (context) =>
                                                                  MainPage(
                                                                      userId: this

                                                                          .userId)),
                                                        );
                                                      },
                                                      child: Text("OK")),
                                                ]);
                                          });
                                    }
                                  },
                                  child: Text("Yes"),
                                ),
                              ],
                            );

                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return alert;
                                });
                          },
                          icon: Icon(Icons.delete_forever)),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
