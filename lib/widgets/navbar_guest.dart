import 'package:flutter/material.dart';
import 'package:pac/screens/login.dart';
import 'package:pac/screens/main_page.dart';

class NavBarGuest extends StatelessWidget {
  final String navtitle;
  //final Task taskClass;

  NavBarGuest({required this.navtitle});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xffCD6858),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(15.0, 25.0, 5.0, 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    this.navtitle,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 24,
                    ),
                  ),
                ),
                Row(
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) =>
                              SignInPage()),
                        );
                      },
                      icon: Icon(
                        Icons.home,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
