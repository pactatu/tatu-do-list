import 'package:flutter/material.dart';
import 'package:pac/screens/login.dart';
import 'package:pac/screens/main_page.dart';

class NavBarLogged extends StatelessWidget {
  final String navtitle;
  final int userId;
  //final Task taskClass;

  NavBarLogged({required this.navtitle, required this.userId});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xffCD6858),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(15.0, 25.0, 5.0, 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    this.navtitle,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 24,
                    ),
                  ),
                ),
                Row(
                  children: [
                    IconButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) =>
                                MainPage(userId: this.userId)),
                          );
                        },
                        icon: Icon(
                            Icons.home,
                            color: Colors.white,
                        ),
                    ),
                    IconButton(
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                title: Text("Are you sure?"),
                                content: Text("Do you want to log out?"),
                                actions: [
                                  TextButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: Text("NO")
                                  ),
                                  TextButton(
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(builder: (context) =>
                                              SignInPage()),
                                        );
                                      },
                                      child: Text("YES")
                                  ),
                                ],
                              );
                            }
                        );
                      },
                      icon: Icon(
                          Icons.logout,
                          color: Colors.white,
                      ),
                    )
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
