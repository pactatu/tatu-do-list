class User {
  final String name;
  final String email;
  final String phone;
  final String username;
  final String password;

  User({required this.name, required this.email, required this.phone, required this.username, required this.password});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      name: json['name'],
      email: json['email'],
      phone: json['phone'],
      username: json['username'],
      password: json['password']
    );
  }

  // static getLoggedIn(Map<String, dynamic> json) {
  //   return json['loggedIn'];
  // }
}