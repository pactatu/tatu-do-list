class Task {
  final int id;
  final String name;
  final String content;
  final int userId;

  Task({required this.id, required this.name, required this.content, required this.userId});

  factory Task.fromJson(Map<String, dynamic> json) {
    return Task(
      id: json['id'],
      name: json['name'],
      content: json['content'],
      userId: json['person_id']
    );
  }
}
