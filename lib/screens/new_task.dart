import 'package:flutter/material.dart';
import 'package:pac/widgets/navbar_logged.dart';

//CLASS TASK
import '../connections/post.dart';
import 'main_page.dart';

class NewTask extends StatefulWidget {
  const NewTask({required this.userId});

  final int userId;

  @override
  _NewTaskState createState() => _NewTaskState(userId: userId);
}

class _NewTaskState extends State<NewTask> {
  _NewTaskState({required this.userId});

  final int userId;
  final _formKey = GlobalKey<FormState>();

  final TextEditingController titleController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();
  var returned;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            NavBarLogged(navtitle: "New Task Page", userId: this.userId),
            Container(
              padding: EdgeInsets.all(20.0),
              child: Form(
                child: Column(
                  key: this._formKey,
                  children: [
                    Container(
                      height: 40.0,
                    ),
                    Container(
                      height: 80.0,
                      child: TextFormField(
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Name is required';
                          }
                          return null;
                        },
                        controller: titleController,
                        decoration: InputDecoration(
                          labelText: "Name",
                          hintText: "Insert the task's name",
                        ),
                      ),
                    ),
                    Container(
                      height: 40.0,
                    ),
                    Container(
                      height: 80.0,
                      child: TextField(
                        controller: descriptionController,
                        decoration: InputDecoration(
                            hintText:
                                "Insert the task's description (optional)",
                            labelText: "Task Description"),
                      ),
                    ),
                    Container(
                      height: 100.0,
                    ),
                    ElevatedButton(
                      onPressed: () async {
                        if (this._formKey.currentState != null) {
                          if (!this._formKey.currentState!.validate()) {
                            return;
                          }
                        }
                        var valueDesc = descriptionController.text;
                        if (descriptionController.value.text.isEmpty) {
                          valueDesc = "";
                        }
                        returned = await createTask(titleController.text, valueDesc, this.userId);

                        if (returned["name"] != null) {
                          ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                  content: Text("Task added!"),
                              ),
                          );
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    MainPage(userId: this.userId)
                            ),
                          );
                        }
                      },
                      child: Text("Add!"),
                      style: ElevatedButton.styleFrom(
                        primary: Color(0xffCFA988),
                        alignment: Alignment.center,
                        textStyle: TextStyle(
                          fontSize: 26.0,
                          color: Colors.white,
                        ),
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
