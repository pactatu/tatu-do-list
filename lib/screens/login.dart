import 'package:async_button_builder/async_button_builder.dart';
import 'package:flutter/material.dart';
import 'package:pac/connections/post.dart';
import 'package:pac/screens/main_page.dart';
import 'package:pac/screens/new_account.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class SignInfo {
  final int userId;

  SignInfo(this.userId);
}

class _SignInPageState extends State<SignInPage> {
  final userController = TextEditingController();
  final passController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomAppBar(
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: Text(
            "©️  Tatu-Do List 2021",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 14.0,
              color: Colors.black45,
            ),
          ),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                color: Color(0xFFcd6858),
                height: 150.0,
                alignment: Alignment.center,
                child: Text(
                  "Tatu-Do List",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'Fruktur',
                    fontSize: 60.0,
                    color: Colors.white,
                  ),
                ),
              ),
              Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 50.0,
                    ),
                    Container(
                      width: 300.0,
                      child: TextFormField(
                        autocorrect: false,
                        controller: userController,
                        decoration: InputDecoration(
                          hintText: "Enter your username",
                          labelText: "Username",
                          // border: OutlineInputBorder(borderRadius: const BorderRadius.all(Radius.circular(4.0))),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "This field is required";
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      height: 20.0,
                    ),
                    Container(
                      width: 300.0,
                      child: TextFormField(
                        autocorrect: false,
                        obscureText: true,
                        controller: passController,
                        decoration: InputDecoration(
                          hintText: "Enter your password",
                          labelText: "password",
                          //border: OutlineInputBorder(borderRadius: const BorderRadius.all(Radius.circular(4.0))),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "This field is required";
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      height: 35.0,
                    ),
                    AsyncButtonBuilder(
                      onPressed: () async {
                        if (!_formKey.currentState!.validate()) {
                          return;
                        }
                        var isLogged = await validateLogin(
                            userController.text, passController.text);
                        if (isLogged['loggedIn']) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    MainPage(userId: isLogged['userId'])),
                          );
                        } else {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                                title: const Text("Login failed"),
                                content: const Text("Invalid credentials"),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, "OK"),
                                    child: const Text("OK"),
                                  )
                                ]),
                          );
                        }
                      },
                      builder: (BuildContext context, Widget child, callback,
                          buttonState) {
                        return ElevatedButton(
                          child: child,
                          onPressed: callback,
                          style: ElevatedButton.styleFrom(
                            primary: Color(0xffCFA988),
                            alignment: Alignment.center,
                            textStyle: TextStyle(
                              fontSize: 17.0,
                              color: Colors.white,
                            ),
                            shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                            ),
                            //fixedSize: Size(80.0, 50.0)),
                          ),
                        );
                      },
                      child: Text('Log in'),
                    ),
                    Container(
                      height: 35.0,
                      alignment: Alignment.center,
                      child: Text(
                        "Or",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 15.0,
                          color: Color(0xffa5a5a5),
                        ),
                      ),
                    ),
                    Container(
                      height: 35.0,
                      alignment: Alignment.topCenter,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Color(0xffCD6858),
                          textStyle: TextStyle(
                            fontSize: 20.0,
                          ),
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                          ),
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SignUpPage()),
                          );
                        },
                        child: Text(
                          "Create an account",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
