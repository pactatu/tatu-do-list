import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pac/connections/post.dart';
import 'package:async_button_builder/async_button_builder.dart';
import 'package:pac/screens/login.dart';
import 'package:pac/widgets/navbar_guest.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _phoneController = TextEditingController();
  final _emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              NavBarGuest(navtitle: "Create an Account Page"),
              Form(
                key: this._formKey,
                child: Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      TextFormField(
                        decoration: const InputDecoration(
                          icon: const Icon(Icons.person),
                          hintText: 'Enter your name (== username if null)',
                          labelText: 'Name',
                        ),
                        validator: (value) {
                          return null;
                        },
                        controller: this._nameController,
                      ),
                      TextFormField(
                        inputFormatters: [
                          FilteringTextInputFormatter.deny(
                              RegExp(r"[^a-zA-Z\!\#\.\_]")),
                        ],
                        decoration: const InputDecoration(
                          icon: const Icon(Icons.person_search),
                          hintText: 'Enter your username (Must be unique)',
                          labelText: 'Username',
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Username is required';
                          }
                          // if (value.contains(RegExp(r"[^a-zA-Z\!\#\.\_]"))) {
                          //   return 'Special characters allowed: ! # . _';
                          // }
                          return null;
                        },
                        controller: this._usernameController,
                      ),
                      TextFormField(
                        obscureText: true,
                        decoration: const InputDecoration(
                          icon: const Icon(Icons.lock),
                          hintText: 'Enter your password',
                          labelText: 'Password',
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Password is required';
                          }
                          if (value.length < 4) {
                            return 'Password too short';
                          }
                          return null;
                        },
                        controller: this._passwordController,
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                          icon: const Icon(Icons.email),
                          hintText: 'Enter your e-mail (optional)',
                          labelText: 'E-mail',
                        ),
                        controller: this._emailController,
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                          icon: const Icon(Icons.phone),
                          hintText: 'Enter your phone (optional)',
                          labelText: 'Phone',
                        ),
                        controller: this._phoneController,
                      ),
                      Container(
                        height: 60.0,
                      ),
                      AsyncButtonBuilder(
                          onPressed: () async {
                            if (!_formKey.currentState!.validate()) {
                              return;
                            }
                            var name = this._nameController.text;
                            var email = this._emailController.text;
                            var phone = this._phoneController.text;

                            if (this._nameController.value.text.isEmpty) {
                              name = this._usernameController.text;
                            }

                            var returned = await createUser(name, this._usernameController.text, this._passwordController.text, email, phone);

                            if (returned["error"] != null) {
                              showDialog(
                                context: context,
                                builder: (context) {
                                  return AlertDialog(
                                    title: Text("Error"),
                                    content: Text(returned["error"]),
                                    actions: [
                                      TextButton(
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                          child: Text("OK")
                                      ),
                                    ],
                                  );
                                }
                              );
                            } else if (returned["username"] != null) {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      title: Text("Success"),
                                      content: Text("Account with username " + returned["username"] + " created"),
                                      actions: [
                                        TextButton(
                                            onPressed: () {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        SignInPage()),
                                                );
                                            },
                                            child: Text("Return to Log In")
                                        ),
                                      ],
                                    );
                                  }
                              );
                            }
                          },
                          builder: (BuildContext context, Widget child, callback, buttonState) {
                            return ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                textStyle: TextStyle(
                                  fontSize: 20.0,
                                ),
                                primary: Color(0xffCD6858),
                              ),
                              child: child,
                              onPressed: callback,
                            );
                          },
                          child: Text('Create user')
                      ),
                      TextButton(
                        style: ElevatedButton.styleFrom(
                          textStyle: TextStyle(
                            fontSize: 15.0,
                          ),
                        ),
                        onPressed: () {
                          this._formKey.currentState?.reset();
                        },
                        child: Text("Reset all fields"),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
