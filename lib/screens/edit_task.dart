import 'package:async_button_builder/async_button_builder.dart';
import 'package:flutter/material.dart';
import 'package:pac/connections/put.dart';
import 'package:pac/model/task_model.dart';
import 'package:pac/widgets/navbar_logged.dart';
import 'main_page.dart';

class EditTask extends StatefulWidget {
  const EditTask({required this.taskInfo});

  final Task taskInfo;

  @override
  _EditTaskState createState() => _EditTaskState(task: taskInfo);
}

class _EditTaskState extends State<EditTask> {
  _EditTaskState({required this.task});

  final Task task;
  final _formKey = GlobalKey<FormState>();

  final TextEditingController titleController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();
  var returned;

  @override
  Widget build(BuildContext context) {
    this.titleController.text = this.task.name;
    this.descriptionController.text = this.task.content;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            NavBarLogged(navtitle: "New Task Page", userId: this.task.userId),
            Container(
              padding: EdgeInsets.all(20.0),
              child: Form(
                child: Column(
                  key: this._formKey,
                  children: [
                    Container(
                      height: 40.0,
                    ),
                    Container(
                      height: 80.0,
                      child: TextFormField(
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Name is required';
                          }
                          return null;
                        },
                        controller: titleController,
                        decoration: InputDecoration(
                          labelText: "Name",
                        ),
                      ),
                    ),
                    Container(
                      height: 40.0,
                    ),
                    Container(
                      height: 80.0,
                      child: TextFormField(
                        controller: descriptionController,
                        decoration: InputDecoration(
                            labelText: "Task Description"
                        ),
                      ),
                    ),
                    Container(
                      height: 100.0,
                    ),
                    AsyncButtonBuilder(
                      onPressed: () async {
                        if (this._formKey.currentState != null) {
                          if (!this._formKey.currentState!.validate()) {
                            return;
                          }
                        }
                        var valueDesc = descriptionController.text;
                        if (descriptionController.value.text.isEmpty) {
                          valueDesc = "";
                        }
                        returned = await updateTask(this.task.id, titleController.text, valueDesc, this.task.userId);

                        if (returned["name"] != null) {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content: Text("Task updated!"),
                            ),
                          );
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    MainPage(userId: this.task.userId)
                            ),
                          );
                        } else if (returned["error"] != null) {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text("Error"),
                                  content: Text("Failed to update task. Error message: " + returned["error"]),
                                  actions: [
                                    TextButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                        child: Text("OK"),
                                    ),
                                  ],
                                );
                              }
                          );
                        }
                      },
                      child: Text("Save"),
                      builder: (BuildContext context, Widget child, callback, buttonState) {
                        return ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Color(0xffCFA988),
                            alignment: Alignment.center,
                            textStyle: TextStyle(
                              fontSize: 26.0,
                              color: Colors.white,
                            ),
                            shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                            ),
                          ),
                          child: child,
                          onPressed: callback,
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
