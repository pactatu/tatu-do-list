import 'package:flutter/material.dart';
import 'package:pac/connections/get.dart';
import 'package:pac/screens/new_task.dart';
import 'package:pac/widgets/navbar_logged.dart';

class MainPage extends StatefulWidget {
  const MainPage({required this.userId});

  final int userId;

  @override
  _MainPageState createState() => _MainPageState(userId: userId);
}

class _MainPageState extends State<MainPage> {

  _MainPageState({required this.userId});

  final int userId;

  late Future<List<dynamic>> futureTask;

  @override
  void initState() {
    super.initState();
    futureTask = fetchTasks(this.userId);
  }

  Future<bool> _onBack() async {
    return await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to log out?'),
        actions: <Widget>[
          new GestureDetector(
            onTap: () => Navigator.of(context).pop(false),
            child: Text("NO"),
          ),
          SizedBox(height: 16),
          new GestureDetector(
            onTap: () => Navigator.of(context).pop(true),
            child: Text("YES"),
          ),
        ],
      ),
    ) ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onBack,
        child: Scaffold(
          backgroundColor: Color(0xfff2f2f2),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              NavBarLogged(navtitle: "Main Page", userId: this.userId),
              Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  "Your tasks",
                  style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 30,
                  ),
                ),
              ),
              FutureBuilder<List<dynamic>>(
                future: futureTask,
                builder: (context, snapshot) {
                  if (snapshot.data == [] && !snapshot.hasError) {
                    return Column(
                      children: [
                        Text("No tasks found! Add one right now!"),
                      ],
                    );
                  } else if (snapshot.hasData && snapshot.data != []) {
                    return Column(
                      children: snapshot.data!.cast<Widget>(),
                    );
                  } else if (snapshot.hasError) {
                    return Text('${snapshot.error}');
                  } else {
                    return Center(child: const CircularProgressIndicator());
                  }
                },
              ),
            ],
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => NewTask(userId: this.userId)),
              );
            },
            child: Icon(Icons.add),
            backgroundColor: Color(0xffCFA988),
          ),
        ),
    );
  }
}
