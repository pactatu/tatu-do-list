import 'dart:convert';

import 'package:http/http.dart' as http;

const url_heroku = "https://banco-pac.herokuapp.com";

//PUT
Future<Map<String, dynamic>> updateTask(int id, String name, String content, int userId) async {
  Map<String, String> headers = {'Content-Type': 'application/json'};

  final response = await http.put(
    Uri.parse(url_heroku + "/todos/" + id.toString()),
    headers: headers,
    body: jsonEncode(<String, String>{
      'name': name,
      'content': content,
      'person_id': userId.toString()
    }),
  );

  try {
    return jsonDecode(response.body);
  } catch (e) {
    return {"error": "Unexpected return from server"};
  }

}