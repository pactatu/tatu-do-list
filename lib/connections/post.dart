import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pac/model/task_model.dart';

const url_heroku = "https://banco-pac.herokuapp.com";

//POST
Future<Map<String, dynamic>> createTask(String name, String content, int userId) async {
  Map<String, String> headers = {'Content-Type': 'application/json'};

  final response = await http.post(
    Uri.parse(url_heroku + "/todos"),
    headers: headers,
    body: jsonEncode(<String, String>{
      'name': name,
      'content': content,
      'person_id': userId.toString()
    }),
  );

  return jsonDecode(response.body);
}

Future<Map<String, dynamic>> createUser(String name, String username, String password, String email, String phone) async {
  Map<String, String> headers = {'Content-Type': 'application/json'};

  final response = await http.post(
    Uri.parse(url_heroku + "/users"),
    headers: headers,
    body: jsonEncode(<String, String>{
      'name': name,
      'username': username,
      'password': password,
      'email': email,
      'phone': phone
    }),
  );

  return jsonDecode(response.body);
}

Future<Map<String, dynamic>> validateLogin(String username, String password) async {
  Map<String, String> headers = {'Content-Type': 'application/json'};

  final response = await http.post(
    Uri.parse(url_heroku + "/users/login"),
    headers: headers,
    body: jsonEncode(<String, String>{
      'username': username,
      'password': password
    }),
  );

  return jsonDecode(response.body);
}