import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pac/model/task_model.dart';
import 'package:pac/model/user_model.dart';
import 'package:pac/widgets/task_card.dart';

const url_heroku = "https://banco-pac.herokuapp.com";

Future<List<dynamic>> fetchTasks(int userId) async {
  final response = await http.get(Uri.parse(url_heroku + '/todos?userId=' + userId.toString()));

  if (response.statusCode == 200) {
    var listOfResp = jsonDecode(response.body);
    var toReturn = [];
    for (int i = 0; i < listOfResp.length; i++) {
      var taskInfo = Task.fromJson(listOfResp[i]);
      toReturn.add(TaskCard(id: taskInfo.id, title: taskInfo.name, description: taskInfo.content, userId: taskInfo.userId));
    }
    return toReturn;

  } else {
    throw Exception('Failed to fetch tasks');
  }
}

Future<User> fetchUser(int userId) async {
  final response = await http.get(Uri.parse(url_heroku + '/todos/' + userId.toString()));

  if (response.statusCode == 200) {
    return User.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to fetch user');
  }
}
