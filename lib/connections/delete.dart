import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pac/model/task_model.dart';
import 'package:pac/model/user_model.dart';

const url_heroku = "https://banco-pac.herokuapp.com";

//DELETE
Future<bool> deleteTask(int taskId) async {
  final response = await http.delete(Uri.parse(url_heroku + "/todos/" + taskId.toString()));

  if (response.statusCode == 204) {
    return true;
  }
  return false;
}